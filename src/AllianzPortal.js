import React, { Component } from 'react';
import './css/AllianzPortal.css';

import NavBar from './components/navbar';
import SideBar from './components/sidebar';
import Content from './components/content';

class AllianzPortal extends Component {
	render() {
		return (
			<div id="reactRoot">
				<NavBar />
				<div id="contentRoot">
					<SideBar />
					<Content />
				</div>
			</div>
		);
	}
}

export default AllianzPortal;