import React, { Component } from 'react';

function SideLinks() {
	let defSize = 40;
	const sideContents = [
		"Dashboard",
		"Client Information",
		"List of Policies",
		"Reports",
		"Other Links",
		"Contact Us"
	];

	const sideGen = sideContents.map(
		sideCntnt =>
			<li className="sideLink">
				<img
					src={"./assets/svg/" + sideCntnt + ".svg"}
					alt={sideCntnt + " icon"}
					height={defSize + "px"}
					width={defSize + "px"}
				/>
				<a href={"/" + sideCntnt}>
					{sideCntnt}
				</a>
			</li>
	);

	return sideGen;
}

class SideBar extends Component {
	render() {
		return (
			<aside id="sideWrapper">
				<nav id="sidebar">
					<ul class="list-unstyled components">
						<SideLinks />
					</ul>
				</nav>
			</aside>
		)
	}
}

export default SideBar;