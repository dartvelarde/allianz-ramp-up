import React, { Component } from 'react';

import Logo from '../assets/img/logo.png';
import SearchIcon from '../assets/icons/search-white.svg';

class NavBar extends Component {
	render() {
		const navIds = ["navBarMain"];

		return (
			<nav class="navbar hd-75" id={navIds[0]}>
				<a class="navbar-brand" href="home.php">
					<img src={Logo} height="40" alt="Allianz logo" />
				</a>
				<form class="form-inline">
					<div class="input-group">
					<input type="text" class="form-control" placeholder="Search for" aria-label="Search" />
					<div class="input-group-prepend">
						<span class="input-group-text navbar-left" id="basic-addon1"><img src={SearchIcon} class="search-icon" alt="Search for" /></span>
					</div>
					</div>
				</form>
			</nav>
		)
	}
}

export default NavBar;